package ${groupId};

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class __mainApp__ {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

}
